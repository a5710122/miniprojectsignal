# imports 
import matplotlib.pyplot as plt 
import numpy as np 
import wave, sys 
  

def visualize(path: str): 
    
    # reading the audio file 
    raw = wave.open(path) 
      
   
    signal = raw.readframes(-1) 
    signal = np.frombuffer(signal, dtype ="int16") 
      
    # gets the frame rate 
    f_rate = raw.getframerate() 
  
    time = np.linspace( 
        0, # start 
        len(signal) / f_rate, 
        num = len(signal) 
    ) 
  
    plt.figure(1)   

    plt.title("Sound Wave") 
    
    plt.xlabel("Time") 
     
    plt.plot(time, signal) 
      
    plt.show() 
  
    # plt.savefig('filename') 
  
  
if __name__ == "__main__": 
    
    path = "recording.wav"
  
    visualize(path) 
